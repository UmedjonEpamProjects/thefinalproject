package com.example.demo.service;


import com.example.demo.dto.Book;

import java.util.List;

public interface BookService {

    List<Book> getAllBook();

}
