package com.example.demo.service;

import com.example.demo.dto.Reader;

public interface SessionReaderService {

    Reader setCurrentSessionReader(Reader reader);

    Reader getCurrentSessionReader();
}
