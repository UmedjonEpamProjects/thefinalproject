package com.example.demo.service;

import com.example.demo.dto.Reader;
import com.example.demo.enums.Role;
import com.example.demo.exception.UserAlreadyExistException;

public interface ReaderService {

    long registerReader(Reader reader) throws UserAlreadyExistException;

    boolean authenticateReader(String login);

    Boolean checkLoginExist(String login);

    Reader findReaderByLogin(String login);

    void addReader(Reader reader);

    void updateReader(long id, String name, String coordinate, long ticket, String login, String password, Role role);

    void removeReader(int ticket);

}
