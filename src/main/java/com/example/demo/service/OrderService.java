package com.example.demo.service;

public interface OrderService {
    void createOrder(long book_id, long reader_id);
}
