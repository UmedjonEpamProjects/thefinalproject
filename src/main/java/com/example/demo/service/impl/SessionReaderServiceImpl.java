package com.example.demo.service.impl;

import com.example.demo.dto.Reader;
import org.springframework.stereotype.Service;

@Service
public class SessionReaderServiceImpl implements com.example.demo.service.SessionReaderService {

    private Reader currentSessionReader;

    @Override
    public Reader setCurrentSessionReader(Reader reader) {
        currentSessionReader = reader;
        return currentSessionReader;
    }

    @Override
    public Reader getCurrentSessionReader() {
        return currentSessionReader;
    }
}