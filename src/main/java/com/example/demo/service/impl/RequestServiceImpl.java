package com.example.demo.service.impl;

import com.example.demo.dto.Request;
import com.example.demo.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RequestServiceImpl implements RequestService{

    @Override
    public Request addNewRequest(Request request) {
        return null;
    }

    @Override
    public List<Request> findRequestByReaderLogin(String login) {
        return null;
    }

    @Override
    public Request findRequestById(Long requestId) {
        return null;
    }
}
