package com.example.demo.service.impl;

import com.example.demo.dto.Reader;
import com.example.demo.enums.Role;
import com.example.demo.exception.UserAlreadyExistException;
import com.example.demo.repository.ReaderRepository;
import com.example.demo.service.ReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class ReaderServiceImpl implements ReaderService {

    @Autowired
    private ReaderRepository readerRepository;



    @Override
    public long registerReader(Reader reader) throws UserAlreadyExistException {
        try {
            System.out.println("try");
            Reader findReaderByLogin = findReaderByLogin(reader.getLogin());
            throw new UserAlreadyExistException("такой ползователь " + reader.getLogin() + " уже существует.");
        } catch (DataAccessException e) {
            System.out.println("catch");
            return readerRepository.registerReader(reader);
        }
    }

    @Override
    public boolean authenticateReader(String login) {
        /*Reader foundReader = readerRepository.findReaderByLogin(login.getLogin());

        if (!foundReader.getPassword().equals(login.getPassword()))
        {
            return null;
        }
        return foundReader;*/
        try {
            readerRepository.findReaderByLogin(login);
            return true;
        } catch (DataAccessException e) {
            return false;
        }
    }


    @Override
    public Boolean checkLoginExist(String login) {
        return null;
    }

    @Override
    public Reader findReaderByLogin(String login) {
        return readerRepository.findReaderByLogin(login);
    }

    @Override
    public void addReader(Reader reader) {
        readerRepository.addReader(reader);
    }

    @Override
    public void updateReader(long id, String name, String coordinate, long ticket, String login, String password, Role role) {

    }

    @Override
    public void removeReader(int ticket) {

    }
}
