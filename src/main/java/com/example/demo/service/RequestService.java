package com.example.demo.service;

import com.example.demo.dto.Request;

import java.util.List;

public interface RequestService {

    Request addNewRequest(Request request);

    List<Request> findRequestByReaderLogin(String login);

    Request findRequestById(Long requestId);



}
