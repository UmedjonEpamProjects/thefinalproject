package com.example.demo.interceptor;

import com.example.demo.dto.Reader;
import com.example.demo.service.impl.SessionReaderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private SessionReaderServiceImpl sessionReaderService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        Reader reader = sessionReaderService.getCurrentSessionReader();

        if (Objects.isNull(reader)) {
            response.sendRedirect("/login");
            return false;
        }

        return true;
    }

}
