package com.example.demo.mappers;

import com.example.demo.dto.Book;
import com.example.demo.dto.Reader;
import com.example.demo.enums.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMapper implements RowMapper<Book> {

    @Override
    public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
        Book book = new Book();
        book.setId(rs.getInt("idbook"));
        book.setBookAuthor(rs.getString("name"));
        book.setBooktitle(rs.getString("outhor"));
        return book;

    }
}
