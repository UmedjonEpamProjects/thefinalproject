package com.example.demo.mappers;

import com.example.demo.dto.Reader;
import com.example.demo.enums.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReaderMapper implements RowMapper<Reader> {

    @Override
    public Reader mapRow(ResultSet rs, int rowNum) throws SQLException {
        Reader reader = new Reader();
        reader.setId(rs.getInt("Id"));
        reader.setName(rs.getString("Name"));
        reader.setTicket(rs.getInt("Ticket"));
        reader.setCoordinate(rs.getString("Coordinate"));
        reader.setCoordinate(rs.getString("Login"));
        reader.setCoordinate(rs.getString("Password"));
        reader.setRole(Role.valueOf(rs.getString("role").toUpperCase()));
        return reader;

    }
}
