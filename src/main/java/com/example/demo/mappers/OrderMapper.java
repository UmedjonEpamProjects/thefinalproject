package com.example.demo.mappers;

import com.example.demo.dto.Book;
import com.example.demo.dto.Order;
import com.example.demo.dto.Reader;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderMapper implements RowMapper<Order> {
    @Override
    public Order mapRow(ResultSet rs, int rowNum) throws SQLException {
        Order order = new Order();
        order.setBook_id(rs.getInt("book_id"));
        order.setReader_id(rs.getInt("reader_id"));
        return order;

    }
}
