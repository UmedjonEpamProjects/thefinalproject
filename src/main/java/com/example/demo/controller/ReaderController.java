package com.example.demo.controller;

import com.example.demo.service.impl.SessionReaderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class ReaderController {

    @Autowired
    private SessionReaderServiceImpl sessionUserService;

    @GetMapping("reader")
    private ModelAndView client(ModelAndView modelAndView) {
        modelAndView.setViewName("Library");
        return modelAndView;
    }

    @GetMapping("reader/book")
    private ModelAndView clientbook(ModelAndView modelAndView) {
        modelAndView.setViewName("book");
        return modelAndView;
    }

    @GetMapping("reader/Authors")
    private ModelAndView clientAuthors(ModelAndView modelAndView) {
        modelAndView.setViewName("Authors");
        return modelAndView;
    }

    @GetMapping("reader/contact")
    private ModelAndView clientcontact(ModelAndView modelAndView) {
        modelAndView.setViewName("contact");
        return modelAndView;
    }

    @GetMapping("reader/aboutus")
    private ModelAndView clientaboutus(ModelAndView modelAndView) {
        modelAndView.setViewName("aboutus");
        return modelAndView;
    }




}
