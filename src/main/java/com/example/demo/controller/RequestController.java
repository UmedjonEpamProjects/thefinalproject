package com.example.demo.controller;

import com.example.demo.dto.Book;
import com.example.demo.service.BookService;
import com.example.demo.service.OrderService;
import com.example.demo.service.RequestService;
import com.example.demo.service.impl.SessionReaderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class RequestController {
    @Autowired
    private SessionReaderServiceImpl sessionUserService;
    @Autowired
    private BookService bookService;
    @Autowired
    private OrderService orderService;

    @GetMapping("reader/fantasy")
    private ModelAndView clientbookfantasy(ModelAndView modelAndView) {
        List<Book> books = bookService.getAllBook();
        modelAndView.addObject("books",books);
        modelAndView.addObject("currentReader", sessionUserService.getCurrentSessionReader());
        modelAndView.setViewName("fantasy");
        return modelAndView;
    }

    @GetMapping("reader/fantasy/{id}")
    private ModelAndView bookfantasy(@PathVariable long id, ModelAndView modelAndView) {
        orderService.createOrder(id,sessionUserService.getCurrentSessionReader().getId());
        modelAndView.addObject("currentReader", sessionUserService.getCurrentSessionReader());
        modelAndView.setViewName("redirect:/reader/fantasy");
        return modelAndView;
    }

}
