package com.example.demo.controller;

import com.example.demo.dto.Reader;
import com.example.demo.exception.UserAlreadyExistException;
import com.example.demo.service.ReaderService;
import com.example.demo.service.SessionReaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private SessionReaderService sessionReaderService;

    @Autowired
    private ReaderService readerService;

    @GetMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView) {
        modelAndView.setViewName("registration");

        return modelAndView;
    }

    @PostMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView, Reader reader, BindingResult result) {
        modelAndView.setViewName("registration");

        try {
            reader.setId((int) readerService.registerReader(reader));
        } catch (UserAlreadyExistException e) {
            modelAndView.addObject("error", "Такой пользователь уже существует");
            return modelAndView;
        }

        sessionReaderService.setCurrentSessionReader(reader);
        modelAndView.addObject("currentReader", reader);
        modelAndView.setViewName("redirect:/reader");
        return modelAndView;
    }

    @GetMapping("logout")
    public ModelAndView logout(ModelAndView modelAndView, HttpServletRequest request) {
        request.getSession().invalidate();
        modelAndView.setViewName("redirect:/login");
        return modelAndView;
    }


    @GetMapping("login")
    public ModelAndView login(ModelAndView modelAndView) {
        modelAndView.setViewName("begin");
        return modelAndView;
    }


    @PostMapping("login")
    public ModelAndView login(ModelAndView modelAndView, Reader reader) {
        Boolean foundReader = readerService.authenticateReader(reader.getLogin());

        if (foundReader) {
            Reader currentReader = readerService.findReaderByLogin(reader.getLogin());
            sessionReaderService.setCurrentSessionReader(currentReader);
            modelAndView.setViewName("redirect:/reader");
            return modelAndView;
        }
        sessionReaderService.setCurrentSessionReader(readerService.findReaderByLogin(reader.getLogin()));
        return new ModelAndView("redirect:/reader" , "reader", foundReader);

    }
}
