package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

public class HomeController {

    @GetMapping("/")
    private ModelAndView homeGet(ModelAndView modelAndView) {
        modelAndView.setViewName("redirect:login");
        return modelAndView;
    }

    @PostMapping("/")
    private ModelAndView homePost(ModelAndView modelAndView) {
        modelAndView.setViewName("redirect:login");
        return modelAndView;
    }
}
