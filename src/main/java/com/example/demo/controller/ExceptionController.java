package com.example.demo.controller;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

public class ExceptionController {

    @ExceptionHandler(RuntimeException.class)
    public ModelAndView runtimeExceptionHandler(RuntimeException re) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("error", re.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }

    /*
     * Обработка RuntimeException
     */
    @ExceptionHandler(Exception.class)
    public ModelAndView exceptionHandler(Exception re) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("error", re.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
