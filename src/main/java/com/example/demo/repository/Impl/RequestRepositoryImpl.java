package com.example.demo.repository.Impl;

import com.example.demo.dto.Request;
import com.example.demo.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class RequestRepositoryImpl implements RequestRepository {

    @Autowired
    DataSource dataSource;

    @Override
    public long addRequest(Request request) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()

                .addValue("book_id", request.getBook_id())
                .addValue("reader_id", request.getReader_id());
        namedParameterJdbcTemplate.update("Insert into reader (book_id, reader_id) Values" +
                "(:book_id, :reader_id)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void removeRequest(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From order Where id = ?", id);
    }


    @Override
    public void updateOrder(Request request) {

    }

    @Override
    public Request getOrderById(long id) {
        return null;
    }
}
