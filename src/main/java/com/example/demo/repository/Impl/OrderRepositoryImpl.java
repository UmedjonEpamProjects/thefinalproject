package com.example.demo.repository.Impl;

import com.example.demo.mappers.OrderMapper;
import com.example.demo.mappers.ReaderMapper;
import com.example.demo.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class OrderRepositoryImpl implements OrderRepository {
    @Autowired
    DataSource dataSource;

    @Override
    public void createOrder(long book_id, long reader_id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("INSERT INTO finalprojectlibrary.order (book_id, reader_id) VALUES(?, ?)", new Object[]{book_id, reader_id});
    }
}
