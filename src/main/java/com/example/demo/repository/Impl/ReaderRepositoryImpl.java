package com.example.demo.repository.Impl;

import com.example.demo.dto.Reader;
import com.example.demo.mappers.ReaderMapper;
import com.example.demo.repository.ReaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
class ReaderRepositoryImpl implements ReaderRepository {


    @Autowired
    DataSource dataSource;

    @Override
    public long registerReader(Reader reader) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()

                .addValue("name", reader.getName())
                .addValue("coordinate", reader.getCoordinate())
                .addValue("ticket", reader.getTicket())
                .addValue("login", reader.getLogin())
                .addValue("password", reader.getPassword())
                .addValue("role", reader.getRole().toString());
        namedParameterJdbcTemplate.update("Insert into reader (name, coordinate, ticket, login, password, role) Values" +
                "(:name, :coordinate, :ticket, :login, :password, :role)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public List<Reader> getAllReaders() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("Select * From reader", new ReaderMapper());
    }

    @Override
    public long addReader(Reader reader) {
        return 0;
    }

    @Override
    public Reader findReaderByLogin(String login) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From reader Where login = ?", new Object[]{login}, new ReaderMapper());
    }

    @Override
    public void updateReader(Reader reader) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Update reader Set Name = ?, Coordinate = ?, Ticket = ?  Where ID = ?",
                new Object[]{reader.getName(), reader.getCoordinate(), reader.getTicket(), reader.getId()});
    }


    @Override
    public void removeReader(int ticket) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("Delete From reader Where Ticket = ?", ticket);
    }

    @Override
    public Reader getReaderByTicket(int ticket) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("Select * From reader Where Ticket = ?", new Object[]{ticket}, new ReaderMapper());
    }

}

