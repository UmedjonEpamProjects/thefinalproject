package com.example.demo.repository;

public interface OrderRepository {
    void createOrder(long book_id, long reader_id);
}
