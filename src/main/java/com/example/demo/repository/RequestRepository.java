package com.example.demo.repository;

import com.example.demo.dto.Request;

public interface RequestRepository {

    long addRequest(Request request);

    void removeRequest(long id);


    void updateOrder(Request request);

    Request getOrderById(long id);

}
