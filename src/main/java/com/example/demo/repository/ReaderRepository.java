package com.example.demo.repository;

import com.example.demo.dto.Reader;
import com.example.demo.exception.UserAlreadyExistException;

import java.util.List;

public interface ReaderRepository {

    long registerReader(Reader reader);

    List<Reader> getAllReaders();

    long addReader(Reader reader);

    Reader findReaderByLogin(String login);

    void updateReader(Reader reader);

    void removeReader(int ticket);

    Reader getReaderByTicket(int ticket);


}