package com.example.demo.dto;

import com.example.demo.enums.Role;

public class Reader {

    private int id;
    private String name;
    private int ticket;
    private String coordinate;
    private String login;
    private String password;
    private Role role = Role.READER;


    public Reader() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTicket() {
        return ticket;
    }

    public void setTicket(int ticket) {
        this.ticket = ticket;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Reader{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", ticket=" + ticket +
                ", coordinate='" + coordinate + '\'' +
                ", login='" + login + '\'' +
                ", password=" + password +
                ", role=" + role +
                '}';
    }
}
