package com.example.demo.dto;

import java.util.List;

public class Request {

    private Long id;
    private String book_id;
    private String reader_id;

    public Request(Long id, String book_id, String reader_id) {
        this.id = id;
        this.book_id = book_id;
        this.reader_id = reader_id;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBook_id() {
        return book_id;
    }

    public void setBook_id(String book_id) {
        this.book_id = book_id;
    }

    public String getReader_id() {
        return reader_id;
    }

    public void setReader_id(String reader_id) {
        this.reader_id = reader_id;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", book_id='" + book_id + '\'' +
                ", reader_id='" + reader_id + '\'' +
                '}';
    }
}
