<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>

<h1>Что-то пошло не так:</h1>

<c:forEach var="error" items="${errors}">
    <p>${error.defaultMessage}</p>
</c:forEach>
</body>
</html>